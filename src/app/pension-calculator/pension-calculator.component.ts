import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { MD_BUTTON_DIRECTIVES } from '@angular2-material/button';
import { MD_SIDENAV_DIRECTIVES } from '@angular2-material/sidenav';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';
import {MdInput} from '@angular2-material/input';
import {MdButton} from '@angular2-material/button';
import {MdToolbar} from '@angular2-material/toolbar';

import { DataMocking, Period, IncomePeriod, OutgoingPeriod, AssetPeriod, DebtPeriod, PeriodPosition, PensionPeriod } from '../services/mocking';


@Component({
  moduleId: module.id,
  selector: 'app-pension-calculator',
  templateUrl: 'pension-calculator.component.html',
  styleUrls: ['pension-calculator.component.css'],
  directives: [
    MD_CARD_DIRECTIVES,
    MD_BUTTON_DIRECTIVES,
    MD_SIDENAV_DIRECTIVES,
    MdInput,
    MdToolbar,
    MdButton,
    MdIcon
  ],
    providers: [MdIconRegistry]
})
export class PensionCalculatorComponent implements OnInit {
  
  isCalculated = false;

    views: Object[] = [
    {
      name: "Pre Retirement Pension Calc",
      description: "Saving to retire"
    },
    {
      name: "Basic Savings Calc",
      description: "Savings accounts etc"
    }
  ]

  pension: Pension = {
    currentBalance: 5000,
    personalContribution: 100,
    employerContribution: 50,
    growthRate: 0.06,
    inflationRate: 0.02,
    currentAge: 24,
    retirementAge: 60,
    compoundYears: 0,
    finalBalance: 0
  };
  constructor() {}

  ngOnInit() {
  }

  yearlyAmounts: YearlyAmount[] = [];

  calculateBalance(pension: Pension) {
    /*
    Compound interest for principal:
    P(1+r/n)^nt
    Future value of a series:
    PMT * (((1 + r/n)^nt - 1) / (r/n))
    Where:
    P = principal amount, PMT = Monthly contribution, r = growthRate, n = paymentFrequency(always 12 for us), t = number of years until retirement
    */
    this.pension.compoundYears = this.pension.retirementAge - this.pension.currentAge;

    for (var i = 0; i <= this.pension.compoundYears; i++) {

      var trueGrowth = this.pension.growthRate - this.pension.inflationRate;
      var monthlyGrowth = (trueGrowth / 12);
      var totalPeriods = 12 * this.pension.compoundYears;
      var periodsToDate = 12 * i;
      var principalValue = (this.pension.currentBalance) * (Math.pow((1 + this.pension.growthRate / 12), periodsToDate));

      //(1 + r/n)^nt
      var totalGrowth = Math.pow(1 + monthlyGrowth, periodsToDate);
      // Gross up the personal contribution by 20% i.e amount/0.8
      var truePersContr = (this.pension.personalContribution / 0.8) * Math.pow((1 + this.pension.inflationRate), i);
      var trueEmpContr = this.pension.employerContribution * Math.pow((1 + this.pension.inflationRate), i) //adjusting for inflation
      var totalContr = +(truePersContr + trueEmpContr).toFixed(2); 

      var seriesValue = (totalContr) * ((totalGrowth - 1) / (monthlyGrowth));

      var finalBalance = +(principalValue + seriesValue).toFixed(2);
      
           
      if(i > 0) {
        var growth = +(finalBalance - this.yearlyAmounts[i - 1].potValue).toFixed(2);
      }

      this.pension.finalBalance = finalBalance;

      this.yearlyAmounts.push({period: i, potValue: finalBalance, growth: growth })

    }
    this.isCalculated = true;
  }

  monthlyGrowth(pension: PensionPeriod) {
    // Current Value
    // Contributions
    // Growth rate
    // Closing value
    // PensionPeriod 
    // period: number,
    // pensionId: number,
    // pensionType: string,
    // pensionName: string,
    // openingValue: number,
    // personalContr: number,
    // employerContr: number,
    // growthRate: number,
    // closingValue: number

  }


}


interface Pension {
  currentBalance: number,
  personalContribution: number,
  employerContribution: number,
  growthRate: number,
  inflationRate: number,
  currentAge: number,
  retirementAge: number,
  compoundYears: number,
  finalBalance: number
}

interface YearlyAmount {
  period: number,
  potValue: number,
  growth: number
}