/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { PensionCalculatorComponent } from './pension-calculator.component';

describe('Component: PensionCalculator', () => {
  it('should create an instance', () => {
    let component = new PensionCalculatorComponent();
    expect(component).toBeTruthy();
  });
});
