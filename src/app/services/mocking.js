"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var DataMocking = (function () {
    function DataMocking() {
        this.periods = [];
    }
    DataMocking.prototype.mockStart = function () {
        //var mockPeriods: Period[] = [];
        //Build a mock starting period and populate with some dummy data
        var period = {
            period: 0,
            income: this.mockIncome(),
            outgoings: this.mockOutgoing(),
            assets: this.mockAsset(),
            pensions: this.mockPension(),
            debts: this.mockDebt(),
            periodPosition: {
                period: 0,
                totalIncome: 0,
                totalOutgoings: 0,
                totalExcessCash: 0,
                totalAssetValue: 0,
                totalDebtValue: 0,
                netWorth: 0
            }
        };
        this.periods.push(period);
        return period;
    };
    DataMocking.prototype.mockIncome = function () {
        var mockIncome = [];
        for (var i = 1; i <= 3; i++) {
            var incomePeriod = {
                period: 0,
                incomeId: i,
                incomeName: "Income " + i,
                incomeType: "Mock",
                incomeAmount: +(Math.random() * 1000).toFixed(0)
            };
            mockIncome.push(incomePeriod);
        }
        return mockIncome;
    };
    DataMocking.prototype.mockOutgoing = function () {
        var mockOutgoings = [];
        var fixedTypes = ["Rent/Mortgage", "Insurances", "Council Tax", "Utility Bills", "Savings"];
        var flexiTypes = ["Clothes", "Going Out", "Holidays", "Gifts", "Leisure"];
        // Fixed Outgoings
        for (var i = 0; i <= 4; i++) {
            var amount;
            if (i === 0) {
                amount = +Math.floor((Math.random() * 500) + 1);
            }
            else {
                amount = +Math.floor((Math.random() * 200) + 1);
            }
            var outgoingPeriod = {
                period: 0,
                outgoingId: i + 1,
                outgoingType: fixedTypes[i],
                outgoingName: fixedTypes[i],
                outgoingAmount: amount
            };
            mockOutgoings.push(outgoingPeriod);
        }
        for (var i = 0; i <= 4; i++) {
            var outgoingPeriod = {
                period: 0,
                outgoingId: i + 6,
                outgoingType: flexiTypes[i],
                outgoingName: flexiTypes[i],
                outgoingAmount: Math.floor(((Math.random() * 200) + 1))
            };
            mockOutgoings.push(outgoingPeriod);
        }
        return mockOutgoings;
    };
    DataMocking.prototype.mockDebt = function () {
        var mockDebt = [];
        var debtTypes = ["Credit Card", "Loan", "Other"];
        for (var i = 1; i <= 5; i++) {
            var intRate = Math.floor(Math.random() * 20);
            var balance = Math.floor(Math.random() * 5000) + 500;
            var payment = +((balance * (intRate / 100 / 12)) + (Math.random() * 10)).toFixed(2);
            var debtPeriod = {
                period: 0,
                debtId: i,
                debtName: "Debt " + i,
                debtType: debtTypes[Math.floor(Math.random() * 2)],
                interestRate: intRate,
                openingBalance: balance,
                monthlyInterest: 0,
                monthlyPayment: payment,
                settled: false,
                closingBalance: balance
            };
            mockDebt.push(debtPeriod);
        }
        return mockDebt;
    };
    DataMocking.prototype.mockAsset = function () {
        var mockAssets = [];
        var assetTypes = ["Current Account", "Savings Account", "Investment", "Other"];
        var rates = [0, 1.2, 6, 1.2];
        for (var i = 0; i < 4; i++) {
            var amount = Math.floor((Math.random() * 5000) + 500);
            var assetPeriod = {
                period: 0,
                assetId: i,
                assetType: assetTypes[i],
                assetName: assetTypes[i],
                openingValue: amount,
                growthRate: rates[i],
                closingValue: amount
            };
            mockAssets.push(assetPeriod);
        }
        return mockAssets;
    };
    DataMocking.prototype.mockPension = function () {
        var mockPension = [];
        var pensionPeriod = {
            period: 0,
            pensionId: 1,
            pensionType: "Work Based Pension",
            pensionName: "Prudential Co-Op",
            openingValue: 15000,
            personalContr: 50,
            employerContr: 25,
            growthRate: 6,
            closingValue: 15000
        };
        return pensionPeriod;
    };
    DataMocking = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-mocking-class',
            template: '<h1>Hello</h1>'
        })
    ], DataMocking);
    return DataMocking;
}());
exports.DataMocking = DataMocking;
//# sourceMappingURL=mocking.js.map