export class DetailsFormModel {
  personalInfo: {
    forename: string;
    middleName: string;
    surname: string;
    dob: string;
    gender: string;
  }

  incomeInfo: {
    jobs: Job[];
    totalIncome: number;
  }

  outGoingsInfo: {
    fixedOutgoings: Outgoing[];
    flexiOutgoings: Outgoing[];
    totalOutgoings: number;
  }

}

export class Job {
  jobTitle: string;
  employerName: string;
  timeAtEmployer: string;
  netIncome: number;
}

export class Outgoing {
  category: string;
  type: string;
  amount: number;
}

export class Asset {
  category: string;
  type: string;
  value: string;
}

export class currentAccount {

}