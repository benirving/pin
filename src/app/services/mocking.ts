import { Component, OnInit } from '@angular/core';
@Component({
    moduleId: module.id,
    selector: 'app-mocking-class',
    template: '<h1>Hello</h1>'
})
export class DataMocking {
    periods: Period[] = [];

    mockStart() {
        //var mockPeriods: Period[] = [];
        //Build a mock starting period and populate with some dummy data
            let period: Period = {
                period: 0,
                income: this.mockIncome(),
                outgoings: this.mockOutgoing(),
                assets: this.mockAsset(),
                pensions: this.mockPension(),
                debts: this.mockDebt(),
                periodPosition: {
                    period: 0,
                    totalIncome: 0,
                    totalOutgoings: 0,
                    totalExcessCash: 0,
                    totalAssetValue: 0,
                    totalDebtValue: 0,
                    netWorth: 0
                }
            };
            this.periods.push(period);
            return period;
    }

    mockIncome() {
        var mockIncome: IncomePeriod[] = [];
        for (var i = 1; i <= 3; i++) {
            let incomePeriod: IncomePeriod = {
                period: 0,
                incomeId: i,
                incomeName: "Income " + i,
                incomeType: "Mock",
                incomeAmount: +(Math.random() * 1000).toFixed(0)
            }
            mockIncome.push(incomePeriod);
        }
        return mockIncome;
    }

    mockOutgoing() {
        var mockOutgoings: OutgoingPeriod[] = [];
        var fixedTypes = ["Rent/Mortgage", "Insurances", "Council Tax", "Utility Bills", "Savings"];
        var flexiTypes = ["Clothes", "Going Out", "Holidays", "Gifts", "Leisure"];

        // Fixed Outgoings
        for (var i = 0; i <= 4; i++) {
            var amount;
            if(i === 0) {
               amount = +Math.floor((Math.random() * 500) +1);
            }
            else { amount = +Math.floor((Math.random() * 200) +1); }
            let outgoingPeriod: OutgoingPeriod = {
                period: 0,
                outgoingId: i+1,
                outgoingType: fixedTypes[i],
                outgoingName: fixedTypes[i],
                outgoingAmount: amount
            }
            mockOutgoings.push(outgoingPeriod);
        }
        for (var i = 0; i <= 4; i++) {
            let outgoingPeriod: OutgoingPeriod = {
                period: 0,
                outgoingId: i + 6,
                outgoingType: flexiTypes[i],
                outgoingName: flexiTypes[i],
                outgoingAmount: Math.floor(((Math.random() * 200) + 1))
            }
            mockOutgoings.push(outgoingPeriod);
        }
        return mockOutgoings;
    }

    mockDebt() {
        var mockDebt: DebtPeriod[] = [];
        var debtTypes = ["Credit Card", "Loan", "Other"]
        
        for (var i = 1; i <= 5; i++) {
            var intRate = Math.floor(Math.random() * 20);
            var balance = Math.floor(Math.random() * 5000) + 500;
            var payment = +((balance * (intRate/100/12)) + (Math.random()*10)).toFixed(2);
            let debtPeriod: DebtPeriod = {
                period: 0,
                debtId: i,
                debtName: "Debt " + i,
                debtType: debtTypes[Math.floor(Math.random() * 2)],
                interestRate: intRate,
                openingBalance: balance,
                monthlyInterest: 0,
                monthlyPayment: payment,
                settled: false,
                closingBalance: balance
            }
            mockDebt.push(debtPeriod);
        }
        return mockDebt;
    }

    mockAsset() {
        var mockAssets = [];
        var assetTypes = ["Current Account", "Savings Account", "Investment", "Other"];
        var rates = [0, 1.2, 6, 1.2];
        for (var i = 0; i < 4; i++) {
            var amount = Math.floor((Math.random() * 5000) + 500);
            let assetPeriod: AssetPeriod = {
                period: 0,
                assetId: i,
                assetType: assetTypes[i],
                assetName: assetTypes[i],
                openingValue: amount,
                growthRate: rates[i],
                closingValue: amount
            }
            mockAssets.push(assetPeriod);
        }
        return mockAssets;
    }

    mockPension() {
        var mockPension = [];
        let pensionPeriod: PensionPeriod = {
            period: 0,
            pensionId: 1,
            pensionType: "Work Based Pension",
            pensionName: "Prudential Co-Op",
            openingValue: 15000,
            personalContr: 50,
            employerContr: 25,
            growthRate: 6,
            closingValue: 15000
        }
        return pensionPeriod;
    }

}

export interface Period {
    period: number;
    income: IncomePeriod[];
    outgoings: OutgoingPeriod[];
    assets: AssetPeriod[];
    pensions: PensionPeriod,
    debts: DebtPeriod[];
    periodPosition: PeriodPosition;
}

export interface PeriodPosition {
    period: number;
    totalIncome: number;
    totalOutgoings: number;
    totalExcessCash: number;
    totalAssetValue: number;
    totalDebtValue: number;
    netWorth: number;
}

export interface IncomePeriod {
    period: number,
    incomeId: number,
    incomeName: string,
    incomeType: string,
    incomeAmount: number,
}

export interface OutgoingPeriod {
    period: number;
    outgoingId: number;
    outgoingType: string;
    outgoingName: string;
    outgoingAmount: number;
}

export interface AssetPeriod {
    period: number;
    assetId: number;
    assetType: string;
    assetName: string;
    openingValue: number;
    growthRate: number;
    closingValue: number;
}

export interface PensionPeriod {
    period: number;
    pensionId: number;
    pensionType: string;
    pensionName: string;
    openingValue: number;
    personalContr: number;
    employerContr: number;
    growthRate: number;
    closingValue: number;
}
   
export interface DebtPeriod {
  period: number;
  debtId: number;
  debtName: string;
  debtType: string;
  interestRate: number;
  openingBalance: number;
  monthlyInterest: number;
  monthlyPayment: number;
  closingBalance: number;
  settled: boolean;

}

