import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { MD_BUTTON_DIRECTIVES } from '@angular2-material/button';
import { MD_SIDENAV_DIRECTIVES } from '@angular2-material/sidenav';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';
import {MdInput} from '@angular2-material/input';
import {MdButton} from '@angular2-material/button';
import {MdToolbar} from '@angular2-material/toolbar';

import {DetailsFormModel, Job, Outgoing} from '../services/detailsForm';
import { Period, IncomePeriod, OutgoingPeriod, AssetPeriod, DebtPeriod } from '../services/mocking';

@Component({
  moduleId: module.id,
  selector: 'app-info-form',
  templateUrl: 'info-form.component.html',
  styleUrls: ['info-form.component.css'],
  directives: [
    MD_CARD_DIRECTIVES,
    MD_BUTTON_DIRECTIVES,
    MD_SIDENAV_DIRECTIVES,
    MdInput,
    MdToolbar,
    MdButton,
    MdIcon
  ],
    providers: [MdIconRegistry]
})
export class InfoFormComponent implements OnInit {
  @Input() form: DetailsFormModel;
  
  constructor() {this.form = new DetailsFormModel(); }

  incomes: Job[] = [{
    jobTitle: "1",
    employerName: "2",
    netIncome: 2000,
    timeAtEmployer: "eqw"
  }];

  incomeInfo = {
    employement: this.incomes,
    totalIncome: 0
  }

  outGoingsInfo: {
    fixedOutgoings: Outgoing[];
    flexiOutgoings: Outgoing[];
    totalOutgoings: number;
  }

  ngOnInit() {
  }

  nextForm(){
    var totalIncome: number = 0; 
    this.incomes.forEach((job) => {
      totalIncome += job.netIncome;
    })
    this.incomeInfo.totalIncome = totalIncome;
    document.getElementById("infoForm").style.display = "none";
    document.getElementById("incomeForm").style.display = "flex";
  }

  prevForm(){
    var totalIncome: number = 0; 
    this.incomes.forEach((job) => {
      totalIncome += job.netIncome;
    })
    this.incomeInfo.totalIncome = totalIncome;
    document.getElementById("infoForm").style.display = "flex";
    document.getElementById("incomeForm").style.display = "none";
  }

}



