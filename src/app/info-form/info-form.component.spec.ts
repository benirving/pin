/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { InfoFormComponent } from './info-form.component';

describe('Component: InfoForm', () => {
  it('should create an instance', () => {
    let component = new InfoFormComponent();
    expect(component).toBeTruthy();
  });
});
