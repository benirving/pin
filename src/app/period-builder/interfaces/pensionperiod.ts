export interface PensionPeriod {
    period: number;
    pensionId: number;
    pensionType: string;
    pensionName: string;
    openingValue: number;
    personalContr: number;
    employerContr: number;
    growthRate: number;
    closingValue: number;
}