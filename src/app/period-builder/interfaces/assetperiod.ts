export interface AssetPeriod {
    period: number;
    assetId: number;
    assetType: string;
    assetName: string;
    openingValue: number;
    growthRate: number;
    closingValue: number;
}