export interface DebtPeriod {
  period: number;
  debtId: number;
  debtName: string;
  debtType: string;
  interestRate: number;
  openingBalance: number;
  monthlyInterest: number;
  monthlyPayment: number;
  closingBalance: number;
  settled: boolean;
}