import {IncomePeriod } from './incomeperiod';
import {OutgoingPeriod} from './outgoingperiod';
import {AssetPeriod} from './assetperiod';
import {DebtPeriod} from './debtperiod';
import {PensionPeriod} from './pensionperiod';
import {PeriodPosition} from './periodposition';

export interface Period {
    period: number;
    income: IncomePeriod[];
    outgoings: OutgoingPeriod[];
    assets: AssetPeriod[];
    pensions: PensionPeriod,
    debts: DebtPeriod[];
    periodPosition: PeriodPosition;
}