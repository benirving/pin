export interface IncomePeriod {
    period: number,
    incomeId: number,
    incomeName: string,
    incomeType: string,
    incomeAmount: number,
}
