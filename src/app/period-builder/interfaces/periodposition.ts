export interface PeriodPosition {
    period: number;
    totalIncome: number;
    totalOutgoings: number;
    totalExcessCash: number;
    totalAssetValue: number;
    totalDebtValue: number;
    netWorth: number;
}