export interface OutgoingPeriod {
    period: number;
    outgoingId: number;
    outgoingType: string;
    outgoingName: string;
    outgoingAmount: number;
}