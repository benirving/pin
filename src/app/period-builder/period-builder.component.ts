//imports
  import { Component, OnInit } from '@angular/core';
  import { MD_CARD_DIRECTIVES } from '@angular2-material/card';

  import { DataMocking } from '../services/mocking';
  import { Period } from './interfaces/period';
  import { AssetPeriod } from './interfaces/assetperiod';
  import { DebtPeriod } from './interfaces/debtperiod';
  import { IncomePeriod } from './interfaces/incomeperiod';
  import { OutgoingPeriod } from './interfaces/outgoingperiod';
  import { PensionPeriod } from './interfaces/pensionperiod';
  import { PeriodPosition } from './interfaces/periodposition';
  import { AngularFire, FirebaseListObservable, FirebaseObjectObservable, AuthProviders, AuthMethods, FirebaseAuth } from 'angularfire2';

//component dec
  @Component({
    moduleId: module.id,
    selector: 'app-period-builder',
    templateUrl: 'period-builder.component.html',
    styleUrls: ['period-builder.component.css'],
    directives: [MD_CARD_DIRECTIVES],
    providers: [DataMocking]
  })
export class PeriodBuilderComponent implements OnInit {
  private initialPeriod: Period;
  private periods: Period[] = [];
  private info: any = {
    _id: 0,
    username: "",
    personalInfo: {
      salutation: "Mr",
      forename: "Benjamin",
      middleNames: "Myles",
      surname: "Irving",
      DOB: "26/06/1992",
      contactInfo: {
        telephone: 44141231,
        addressLine1: "29 Parliament Street",
        city: "Liverpool",
        postcode: "L8 5RN",
      },
      livingArrangement: "Living with partner",
      children: 0
    },
    financialInfo: {}
  };
  private userInfo: any = {}
  private infoJson: any;
  private logged: any;
  private loggedUser: FirebaseObjectObservable<any[]>;
  private loggedUserInfo: any;
  private user: FirebaseObjectObservable<any[]>;

  constructor(private mocker: DataMocking,
    private af: AngularFire,
    private _auth: FirebaseAuth) {
    this.logged = this.af.auth.subscribe(auth => {
      if (auth) {
        this.logged = true;
        //this.loggedUser = auth.uid;
        this.user = af.database.object('users/' + this.loggedUser);
        console.log(auth);
        this.user.forEach((p) => {
          this.loggedUserInfo = p;
          console.log(p);
        })
      }
      else {
        this.logged = false;
        console.log(auth);
      }
    });
  }

  ngOnInit() {
    //this.buildPeriods(this.initialPeriod, 1);
    // this.infoJson = JSON.stringify(this.info, null, 4);
    // document.body.innerHTML = "";
    // document.body.appendChild(document.createTextNode(JSON.stringify(this.info, null, 4)));
  }

  private buildPeriods(initialPeriod: Period, years: number) {
    this.periods[0] = initialPeriod;
    let periods: number = years * 12;
    var firstPeriod: Period = {
      period: 1,
      income: [],
      outgoings: [],
      assets: [],
      pensions: initialPeriod.pensions,
      debts: [],
      periodPosition: {
        period: initialPeriod.period + 1,
        totalIncome: 0,
        totalOutgoings: 0,
        totalExcessCash: 0,
        totalAssetValue: 0,
        totalDebtValue: 0,
        netWorth: 0
      }
    }
    initialPeriod.assets.forEach((asset) => {
      var growth = this.calcInterest(asset.growthRate, asset.closingValue);
      var closingValue = +(asset.closingValue + growth).toFixed(2);
      let nextPeriod: AssetPeriod = {
        period: asset.period + 1,
        assetId: asset.assetId,
        assetType: asset.assetType,
        assetName: asset.assetName,
        openingValue: asset.closingValue,
        growthRate: asset.growthRate,
        closingValue: closingValue
      }
      firstPeriod.assets.push(nextPeriod);
      firstPeriod.periodPosition.totalAssetValue += asset.closingValue;
    })

    initialPeriod.debts.forEach((debt) => {
      var growth = this.calcInterest(debt.monthlyInterest, debt.closingBalance);
      var adjBalance = debt.closingBalance + growth;
      var payment = this.calcPayment(debt.monthlyPayment, adjBalance);
      var closingBalance = +(adjBalance - payment).toFixed(2);
      var settled = (closingBalance === 0);
      let nextPeriod: DebtPeriod = {
        period: debt.period + 1,
        debtId: debt.debtId,
        debtName: debt.debtName,
        debtType: debt.debtType,
        interestRate: debt.interestRate,
        openingBalance: debt.closingBalance,
        monthlyInterest: growth,
        monthlyPayment: payment,
        closingBalance: closingBalance,
        settled: settled
      }
      firstPeriod.debts.push(nextPeriod);
      firstPeriod.periodPosition.totalDebtValue += debt.closingBalance;
    })

    initialPeriod.income.forEach((income) => {
      let nextPeriod: IncomePeriod = income;
      firstPeriod.income.push(nextPeriod);
      firstPeriod.periodPosition.totalIncome += income.incomeAmount;
    })

    initialPeriod.outgoings.forEach((outgoing) => {
      let nextPeriod: OutgoingPeriod = outgoing;
      firstPeriod.outgoings.push(nextPeriod);
      firstPeriod.periodPosition.totalOutgoings += outgoing.outgoingAmount;
    })

    firstPeriod.periodPosition.totalExcessCash = firstPeriod.periodPosition.totalIncome - firstPeriod.periodPosition.totalOutgoings;
    firstPeriod.periodPosition.netWorth = firstPeriod.periodPosition.totalAssetValue - firstPeriod.periodPosition.totalDebtValue;
    this.periods.push(firstPeriod);
    this.userInfo.financialInfo.initialPeriod = initialPeriod;
    this.userInfo.financialInfo["period 1"] = firstPeriod;

    for (var i = 2; i <= periods; i++) {
      var thisPeriod: Period = {
        period: i,
        income: [],
        outgoings: [],
        assets: [],
        pensions: initialPeriod.pensions,
        debts: [],
        periodPosition: {
          period: i,
          totalIncome: 0,
          totalOutgoings: 0,
          totalAssetValue: 0,
          totalDebtValue: 0,
          netWorth: 0,
          totalExcessCash: 0
        }
      }

      var lastPeriod = this.periods[i - 1];
      var lastPeriodIncome = lastPeriod.income;
      var lastPeriodOutgoings = lastPeriod.outgoings;
      var lastPeriodAssets = lastPeriod.assets;
      var lastPeriodDebts = lastPeriod.debts;

      lastPeriod.income.forEach((income) => {
        let nextPeriod: IncomePeriod = income;
        thisPeriod.income.push(nextPeriod);
        thisPeriod.periodPosition.totalIncome += income.incomeAmount;
      })

      lastPeriod.outgoings.forEach((outgoing) => {
        let nextPeriod: OutgoingPeriod = outgoing;
        thisPeriod.outgoings.push(nextPeriod);
        thisPeriod.periodPosition.totalOutgoings += outgoing.outgoingAmount;
      })

      lastPeriod.assets.forEach((asset) => {
        var growth = this.calcInterest(asset.growthRate, asset.closingValue);
        var closingValue = +(asset.closingValue + growth).toFixed(2);
        let nextPeriod: AssetPeriod = {
          period: asset.period + 1,
          assetId: asset.assetId,
          assetType: asset.assetType,
          assetName: asset.assetName,
          openingValue: asset.closingValue,
          growthRate: asset.growthRate,
          closingValue: closingValue
        }
        thisPeriod.assets.push(nextPeriod);
        thisPeriod.periodPosition.totalAssetValue += closingValue;
      })

      lastPeriod.debts.forEach((debt) => {
        var growth = this.calcInterest(debt.monthlyInterest, debt.closingBalance);
        var adjBalance = debt.closingBalance + growth;
        var payment = this.calcPayment(debt.monthlyPayment, adjBalance);
        var closingBalance = +(adjBalance - payment).toFixed(2);
        var settled = (closingBalance === 0);
        let nextPeriod: DebtPeriod = {
          period: debt.period + 1,
          debtId: debt.debtId,
          debtName: debt.debtName,
          debtType: debt.debtType,
          interestRate: debt.interestRate,
          openingBalance: debt.closingBalance,
          monthlyInterest: growth,
          monthlyPayment: payment,
          closingBalance: closingBalance,
          settled: settled
        }
        if (!settled) {
        thisPeriod.debts.push(nextPeriod);
        thisPeriod.periodPosition.totalDebtValue += debt.closingBalance;
        }
      })
      thisPeriod.periodPosition.totalExcessCash = thisPeriod.periodPosition.totalIncome - thisPeriod.periodPosition.totalOutgoings;
      thisPeriod.periodPosition.netWorth = (thisPeriod.periodPosition.totalAssetValue - thisPeriod.periodPosition.totalDebtValue);
      this.periods.push(thisPeriod);
      var periodName = "period " + i;
      this.userInfo.financialInfo["period " + i] = thisPeriod;
    }
    console.log(this.periods);
    //document.body.appendChild(document.createTextNode(JSON.stringify(this.info, null, 4)));

  }
  private calcInterest(interestRate: number, balance: number) {
    var periodInterest: number = (interestRate / 12) / 100;
    var periodGrowth = balance * periodInterest;
    return periodGrowth;
  }
  private calcPayment(lastPayment: number, adjBalance: number) {
    if (adjBalance > 0) {
      var payment = +(Math.min(adjBalance, lastPayment)).toFixed(2);
      return payment;
    }
  }

  // membership methods
  login() {
    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    }).then((authData) => {
      this.loggedUser = this.af.database.object('users/' + authData.uid);
      this.userInfo = this.info;
      this.userInfo._id = authData.uid;
      this.userInfo.username = authData.auth.providerData[0].dislayName || authData.auth.email;
      let initialPeriod = this.mocker.mockStart();
      this.userInfo.financialInfo.initialPeriod = initialPeriod;
      this.loggedUser.update(this.userInfo);
      this.buildPeriods(initialPeriod, 75);
      this.loggedUser.update(this.userInfo);
      console.log(this.loggedUser);
    })
    this.af.auth.subscribe(auth => {
      if (auth) {
        console.log("logged in");
      }
    });

  }

  logOut() {
    console.log("logged out");
    this.af.auth.logout();
    this.userInfo = null;
  }

  addInfo() {
    debugger;
    //var uid = this.loggedUser.uid;
    var obj = {
      uid: this.info
    }
    // this.users.push({
    //   obj
    //   //_id: this.loggedUser
    // }).then(console.log("pushed"));
  }
}

interface UserInfo {
  $key: any;
  _id: any;
  financialInfo: {};
  personalInfo: {};
  username: any;
}
