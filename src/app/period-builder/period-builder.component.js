"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var card_1 = require('@angular2-material/card');
var mocking_1 = require('../services/mocking');
var angularfire2_1 = require('angularfire2');
var PeriodBuilderComponent = (function () {
    function PeriodBuilderComponent(mocker, af, _auth) {
        var _this = this;
        this.mocker = mocker;
        this.af = af;
        this._auth = _auth;
        this.periods = [];
        this.info = {
            _id: 0,
            username: "",
            personalInfo: {
                salutation: "Mr",
                forename: "Benjamin",
                middleNames: "Myles",
                surname: "Irving",
                DOB: "26/06/1992",
                contactInfo: {
                    telephone: 44141231,
                    addressLine1: "29 Parliament Street",
                    city: "Liverpool",
                    postcode: "L8 5RN"
                },
                livingArrangement: "Living with partner",
                children: 0
            },
            financialInfo: {}
        };
        this.userInfo = {};
        this.logged = this.af.auth.subscribe(function (auth) {
            if (auth) {
                _this.logged = true;
                //this.loggedUser = auth.uid;
                _this.user = af.database.object('users/' + _this.loggedUser);
                console.log(auth);
                _this.user.forEach(function (p) {
                    _this.loggedUserInfo = p;
                    console.log(p);
                });
            }
            else {
                _this.logged = false;
                console.log(auth);
            }
        });
    }
    PeriodBuilderComponent.prototype.ngOnInit = function () {
        //this.buildPeriods(this.initialPeriod, 1);
        // this.infoJson = JSON.stringify(this.info, null, 4);
        // document.body.innerHTML = "";
        // document.body.appendChild(document.createTextNode(JSON.stringify(this.info, null, 4)));
    };
    PeriodBuilderComponent.prototype.buildPeriods = function (initialPeriod, years) {
        var _this = this;
        this.periods[0] = initialPeriod;
        var periods = years * 12;
        var firstPeriod = {
            period: 1,
            income: [],
            outgoings: [],
            assets: [],
            pensions: initialPeriod.pensions,
            debts: [],
            periodPosition: {
                period: initialPeriod.period + 1,
                totalIncome: 0,
                totalOutgoings: 0,
                totalExcessCash: 0,
                totalAssetValue: 0,
                totalDebtValue: 0,
                netWorth: 0
            }
        };
        initialPeriod.assets.forEach(function (asset) {
            var growth = _this.calcInterest(asset.growthRate, asset.closingValue);
            var closingValue = +(asset.closingValue + growth).toFixed(2);
            var nextPeriod = {
                period: asset.period + 1,
                assetId: asset.assetId,
                assetType: asset.assetType,
                assetName: asset.assetName,
                openingValue: asset.closingValue,
                growthRate: asset.growthRate,
                closingValue: closingValue
            };
            firstPeriod.assets.push(nextPeriod);
            firstPeriod.periodPosition.totalAssetValue += asset.closingValue;
        });
        initialPeriod.debts.forEach(function (debt) {
            var growth = _this.calcInterest(debt.monthlyInterest, debt.closingBalance);
            var adjBalance = debt.closingBalance + growth;
            var payment = _this.calcPayment(debt.monthlyPayment, adjBalance);
            var closingBalance = +(adjBalance - payment).toFixed(2);
            var settled = (closingBalance === 0);
            var nextPeriod = {
                period: debt.period + 1,
                debtId: debt.debtId,
                debtName: debt.debtName,
                debtType: debt.debtType,
                interestRate: debt.interestRate,
                openingBalance: debt.closingBalance,
                monthlyInterest: growth,
                monthlyPayment: payment,
                closingBalance: closingBalance,
                settled: settled
            };
            firstPeriod.debts.push(nextPeriod);
            firstPeriod.periodPosition.totalDebtValue += debt.closingBalance;
        });
        initialPeriod.income.forEach(function (income) {
            var nextPeriod = income;
            firstPeriod.income.push(nextPeriod);
            firstPeriod.periodPosition.totalIncome += income.incomeAmount;
        });
        initialPeriod.outgoings.forEach(function (outgoing) {
            var nextPeriod = outgoing;
            firstPeriod.outgoings.push(nextPeriod);
            firstPeriod.periodPosition.totalOutgoings += outgoing.outgoingAmount;
        });
        firstPeriod.periodPosition.totalExcessCash = firstPeriod.periodPosition.totalIncome - firstPeriod.periodPosition.totalOutgoings;
        firstPeriod.periodPosition.netWorth = firstPeriod.periodPosition.totalAssetValue - firstPeriod.periodPosition.totalDebtValue;
        this.periods.push(firstPeriod);
        this.userInfo.financialInfo.initialPeriod = initialPeriod;
        this.userInfo.financialInfo["period 1"] = firstPeriod;
        for (var i = 2; i <= periods; i++) {
            var thisPeriod = {
                period: i,
                income: [],
                outgoings: [],
                assets: [],
                pensions: initialPeriod.pensions,
                debts: [],
                periodPosition: {
                    period: i,
                    totalIncome: 0,
                    totalOutgoings: 0,
                    totalAssetValue: 0,
                    totalDebtValue: 0,
                    netWorth: 0,
                    totalExcessCash: 0
                }
            };
            var lastPeriod = this.periods[i - 1];
            var lastPeriodIncome = lastPeriod.income;
            var lastPeriodOutgoings = lastPeriod.outgoings;
            var lastPeriodAssets = lastPeriod.assets;
            var lastPeriodDebts = lastPeriod.debts;
            lastPeriod.income.forEach(function (income) {
                var nextPeriod = income;
                thisPeriod.income.push(nextPeriod);
                thisPeriod.periodPosition.totalIncome += income.incomeAmount;
            });
            lastPeriod.outgoings.forEach(function (outgoing) {
                var nextPeriod = outgoing;
                thisPeriod.outgoings.push(nextPeriod);
                thisPeriod.periodPosition.totalOutgoings += outgoing.outgoingAmount;
            });
            lastPeriod.assets.forEach(function (asset) {
                var growth = _this.calcInterest(asset.growthRate, asset.closingValue);
                var closingValue = +(asset.closingValue + growth).toFixed(2);
                var nextPeriod = {
                    period: asset.period + 1,
                    assetId: asset.assetId,
                    assetType: asset.assetType,
                    assetName: asset.assetName,
                    openingValue: asset.closingValue,
                    growthRate: asset.growthRate,
                    closingValue: closingValue
                };
                thisPeriod.assets.push(nextPeriod);
                thisPeriod.periodPosition.totalAssetValue += closingValue;
            });
            lastPeriod.debts.forEach(function (debt) {
                var growth = _this.calcInterest(debt.monthlyInterest, debt.closingBalance);
                var adjBalance = debt.closingBalance + growth;
                var payment = _this.calcPayment(debt.monthlyPayment, adjBalance);
                var closingBalance = +(adjBalance - payment).toFixed(2);
                var settled = (closingBalance === 0);
                var nextPeriod = {
                    period: debt.period + 1,
                    debtId: debt.debtId,
                    debtName: debt.debtName,
                    debtType: debt.debtType,
                    interestRate: debt.interestRate,
                    openingBalance: debt.closingBalance,
                    monthlyInterest: growth,
                    monthlyPayment: payment,
                    closingBalance: closingBalance,
                    settled: settled
                };
                if (!settled) {
                    thisPeriod.debts.push(nextPeriod);
                    thisPeriod.periodPosition.totalDebtValue += debt.closingBalance;
                }
            });
            thisPeriod.periodPosition.totalExcessCash = thisPeriod.periodPosition.totalIncome - thisPeriod.periodPosition.totalOutgoings;
            thisPeriod.periodPosition.netWorth = (thisPeriod.periodPosition.totalAssetValue - thisPeriod.periodPosition.totalDebtValue);
            this.periods.push(thisPeriod);
            var periodName = "period " + i;
            this.userInfo.financialInfo["period " + i] = thisPeriod;
        }
        console.log(this.periods);
        //document.body.appendChild(document.createTextNode(JSON.stringify(this.info, null, 4)));
    };
    PeriodBuilderComponent.prototype.calcInterest = function (interestRate, balance) {
        var periodInterest = (interestRate / 12) / 100;
        var periodGrowth = balance * periodInterest;
        return periodGrowth;
    };
    PeriodBuilderComponent.prototype.calcPayment = function (lastPayment, adjBalance) {
        if (adjBalance > 0) {
            var payment = +(Math.min(adjBalance, lastPayment)).toFixed(2);
            return payment;
        }
    };
    // membership methods
    PeriodBuilderComponent.prototype.login = function () {
        var _this = this;
        this.af.auth.login({
            provider: angularfire2_1.AuthProviders.Google,
            method: angularfire2_1.AuthMethods.Popup
        }).then(function (authData) {
            _this.loggedUser = _this.af.database.object('users/' + authData.uid);
            _this.userInfo = _this.info;
            _this.userInfo._id = authData.uid;
            _this.userInfo.username = authData.auth.providerData[0].dislayName || authData.auth.email;
            var initialPeriod = _this.mocker.mockStart();
            _this.userInfo.financialInfo.initialPeriod = initialPeriod;
            _this.loggedUser.update(_this.userInfo);
            _this.buildPeriods(initialPeriod, 76);
            _this.loggedUser.update(_this.userInfo);
            console.log(_this.loggedUser);
        });
        this.af.auth.subscribe(function (auth) {
            if (auth) {
                console.log("logged in");
            }
        });
    };
    PeriodBuilderComponent.prototype.logOut = function () {
        console.log("logged out");
        this.af.auth.logout();
        this.userInfo = null;
    };
    PeriodBuilderComponent.prototype.addInfo = function () {
        debugger;
        //var uid = this.loggedUser.uid;
        var obj = {
            uid: this.info
        };
        // this.users.push({
        //   obj
        //   //_id: this.loggedUser
        // }).then(console.log("pushed"));
    };
    PeriodBuilderComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'app-period-builder',
            templateUrl: 'period-builder.component.html',
            styleUrls: ['period-builder.component.css'],
            directives: [card_1.MD_CARD_DIRECTIVES],
            providers: [mocking_1.DataMocking]
        })
    ], PeriodBuilderComponent);
    return PeriodBuilderComponent;
}());
exports.PeriodBuilderComponent = PeriodBuilderComponent;
//# sourceMappingURL=period-builder.component.js.map