/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { PeriodBuilderComponent } from './period-builder.component';

// describe('Component: PeriodBuilder', () => {
//   it('should create an instance', () => {
//     let component = new PeriodBuilderComponent();
//     expect(component).toBeTruthy();
//   });
// });
