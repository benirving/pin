/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {
  beforeEach, beforeEachProviders,
  describe, xdescribe,
  expect, it, xit,
  async, inject
} from '@angular/core/testing';

import { DebtFreeCalculatorComponent } from './debt-free-calculator.component';

// describe('Component: DebtFreeCalculator', () => {
//   it('should create an instance', () => {
//     let component = new DebtFreeCalculatorComponent();
//     expect(component).toBeTruthy();
//   });
// });
