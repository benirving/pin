import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MD_CARD_DIRECTIVES } from '@angular2-material/card';
import { MD_BUTTON_DIRECTIVES } from '@angular2-material/button';
import { MD_SIDENAV_DIRECTIVES } from '@angular2-material/sidenav';
import {MdIcon, MdIconRegistry} from '@angular2-material/icon';
import {MdInput} from '@angular2-material/input';
import {MdButton} from '@angular2-material/button';
import {MdToolbar} from '@angular2-material/toolbar';
import { PeriodBuilderComponent } from '../period-builder/period-builder.component';


@Component({
  moduleId: module.id,
  selector: 'app-debt-free-calculator',
  templateUrl: 'debt-free-calculator.component.html',
  styleUrls: ['debt-free-calculator.component.css'],
  directives: [
    MD_CARD_DIRECTIVES,
    MD_BUTTON_DIRECTIVES,
    MD_SIDENAV_DIRECTIVES,
    MdInput,
    MdToolbar,
    MdButton,
    MdIcon
  ],
  providers: [MdIconRegistry, PeriodBuilderComponent]
})
export class DebtFreeCalculatorComponent implements OnInit {
  //@Input() userModel: UserModel;
  debtFreeCalc: boolean = false;
  debtFreePossible: boolean;
  monthsRemaining: number = 0;

  debts: Debt[] = [
    {
      _id: 1,
      name: "Debt 1",
      amount: 1000,
      interestRate: 12,
      monthlyPayment: 11,
      monthsRemaining: null,
      totalInterest: null
    }
  ]

  monthlyPeriods: PeriodDebt[] = [];
  periods: Period[] = [];
  constructor(private periodBuilder: PeriodBuilderComponent) { }

  ngOnInit() {
    debugger;
    
   }

  /* We basically want to, in each period, in order of interest % of debt (perhaps):
    For Each Debt
      Calculate interest for debt from opening balance
      Calculate payment (Min of monthly or remaining balance)
      Add interest and subtract payment from opening balance to set closing balance
      Check if debt has been paid settled
      Go To Next Debt
    Go To Next Period.   
  */

  debtCalcInterest(interestRate: number, balance: number) {
    var periodInterest: number = (interestRate / 12) / 100;
    var periodGrowth = balance * periodInterest;
    return periodGrowth;
  }

  debtCalcPayment(lastPayment: number, adjBalance: number) {
    if (adjBalance > 0) {
      var payment = Math.min(adjBalance, lastPayment);
      return payment;
    }
  }

  handleDebt() {
    // Set up period 0 to work from based on debt info given by user
    var initialPeriod: Period = { period: 0, debts: [] }
    var firstPeriod: Period = { period: 1, debts: [] }
    this.debts.forEach((debt) => {
      let debtPeriod: DebtPeriod = {
        period: 0,
        debtId: debt._id,
        debtName: debt.name,
        interestRate: debt.interestRate,
        openingBalance: debt.amount,
        monthlyInterest: 0,
        monthlyPayment: debt.monthlyPayment,
        settled: false,
        closingBalance: debt.amount
      }
      let nextPeriod: DebtPeriod = {
        period: debtPeriod.period + 1,
        debtId: debtPeriod.debtId,
        debtName: debtPeriod.debtName,
        interestRate: debt.interestRate,
        openingBalance: debtPeriod.closingBalance,
        monthlyInterest: 0,
        monthlyPayment: 0,
        settled: false,
        closingBalance: 0
      }
      initialPeriod.debts.push(debtPeriod);
      firstPeriod.debts.push(nextPeriod);
    })
    //this.userModel.periods[0].debts.push(initialPeriod);
    this.periods.push(initialPeriod);
    this.periods.push(firstPeriod);

    // In this loop of Periods we create a debtPeriod for each debt and do suitable calculations
    for (var i = 1; i < this.monthsRemaining; i++) {

      var lastPeriod = i - 1;
      var lastPeriodDebts = this.periods[lastPeriod].debts;
      var tempArr = [];
      // this.periods[i] = {period: i, debts: []};
      lastPeriodDebts.forEach((debtPeriod) => {
        if (!debtPeriod.settled) {
          var monthlyInterest = this.debtCalcInterest(debtPeriod.interestRate, debtPeriod.closingBalance);
          var intAdjBalance = debtPeriod.closingBalance + monthlyInterest;
          var monthlyPayment = this.debtCalcPayment(debtPeriod.monthlyPayment, intAdjBalance);
          var payAdjBalance = intAdjBalance - monthlyPayment;
          var settled = (payAdjBalance === 0);
          let nextPeriod: DebtPeriod = {
            period: debtPeriod.period + 1,
            debtId: debtPeriod.debtId,
            debtName: debtPeriod.debtName,
            interestRate: debtPeriod.interestRate,
            openingBalance: debtPeriod.closingBalance,
            monthlyInterest: monthlyInterest,
            monthlyPayment: monthlyPayment,
            settled: settled,
            closingBalance: payAdjBalance
          }
          tempArr.push(nextPeriod);
        }
      })
      this.periods[i] = { period: i, debts: tempArr }
    }
    debugger;
  }



  // This is purely for displaying visually when a user will be debt free, calcs dont pay off debts
  calcDebtFree() {
    debugger;
    this.monthsRemaining = 0;
    this.debts.forEach((debt) => {
      //this.debtAddInterest(debt);
      debt.monthsRemaining = 0;
      var amount = debt.amount;
      var monthlyGrowth = debt.interestRate / 100 / 12;
      //var monthlyInterest = debt.amount * monthlyGrowth;
      while (amount >= 0) {
        var monthlyInterest = amount * monthlyGrowth;
        if (debt.monthlyPayment <= monthlyInterest) {
          console.log("You'll never pay this off");
          debt.monthsRemaining = Number.POSITIVE_INFINITY;
          break;
        }
        amount += monthlyInterest;
        amount -= debt.monthlyPayment;
        debt.monthsRemaining++;
      }
    })
    this.maxMonthsRemaining();
    this.handleDebt();
    this.excessIncome();
    this.debtFreeCalc = true;
  }


  maxMonthsRemaining() {
    this.debts.forEach((debt) => {
      if (debt.monthsRemaining > this.monthsRemaining) {
        this.monthsRemaining = debt.monthsRemaining;
        return;
      }
      else return;
    })
    if (this.monthsRemaining < Number.POSITIVE_INFINITY) { this.debtFreePossible = true; };
  }

  excessIncome() {
    // Check each debt against the debt that takes longest to pay off
    // Figure out when it is paid off and then how much monthly cash becomes available

    // Build out array of x periods where x is time to pay off longest debt
    //this.monthlyPeriods = [(this.monthsRemaining)];

    // Iterate through each period, calculate any spare cash

    for (var i = 0; i < this.monthsRemaining; i++) {
      var thisPeriodDebt: MonthlyDebt[] = [];
      var tempArr = [];
      //var periodDebt: number;
      var periodExcess: number = 0;

      this.debts.forEach((debt) => {
        debugger;
        var excess: MonthlyDebt = { period: i, debtName: debt.name, monthlyExcess: 0 };

        // Checking if debt has been paid off: TO:DO move this to higher tier function and reduce debt amount each period with payment
        if (debt.monthsRemaining <= i) {
          excess.monthlyExcess = debt.monthlyPayment
          periodExcess += debt.monthlyPayment;
        }
        thisPeriodDebt.push(excess);
      })
      tempArr.push(thisPeriodDebt);
      let thisPeriod: PeriodDebt = { period: i, debts: thisPeriodDebt, totalExcess: periodExcess };
      this.monthlyPeriods.push(thisPeriod);
      console.log("In month " + i + " you have: £" + periodExcess + " spare cash.");
    }
    console.log("After " + i + " months you will have £" + periodExcess + " spare cash and be debt free!");
  }

  addMoreDebt() {
    let debt: Debt = {
      _id: this.debts.length + 1,
      name: null,
      amount: null,
      interestRate: null,
      monthlyPayment: null,
      monthsRemaining: null,
      totalInterest: null
    }
    this.debts.push(debt);
  }
}

interface Period {
  period: number,
  debts: DebtPeriod[],
}

interface PeriodDebt {
  period: number;
  debts: MonthlyDebt[];
  totalExcess: number
}

interface DebtPeriod {
  period: number,
  debtId: number,
  debtName: string,
  interestRate: number,
  openingBalance: number,
  monthlyInterest: number,
  monthlyPayment: number,
  settled: boolean,
  closingBalance: number
}

interface MonthlyDebt {
  period: number,
  //debtId: number,
  debtName: string,
  //monthlyInterest: number,
  //monthsRemaining: number,
  monthlyExcess: number
}

interface Debt {
  _id: number,
  name: string;
  amount: number;
  interestRate: number,
  monthlyPayment: number,
  monthsRemaining: number,
  totalInterest: number
}