import { Component, OnInit } from '@angular/core';
import { PensionCalculatorComponent } from './pension-calculator/pension-calculator.component';
import { InfoFormComponent } from './info-form/info-form.component';
import { DebtFreeCalculatorComponent } from './debt-free-calculator/debt-free-calculator.component';
import { DataMocking } from './services/mocking';
import { PeriodBuilderComponent } from './period-builder/period-builder.component';


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [
    PensionCalculatorComponent,
    InfoFormComponent,
    DebtFreeCalculatorComponent,
    PeriodBuilderComponent
    ],
    providers: [DataMocking]
})
export class AppComponent implements OnInit {
  title = 'app works!';

  ngOnInit() {
  };
}
