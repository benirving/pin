import { PinpointPage } from './app.po';

describe('pinpoint App', function() {
  let page: PinpointPage;

  beforeEach(() => {
    page = new PinpointPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
